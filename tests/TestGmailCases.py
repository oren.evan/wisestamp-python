import unittest
from time import sleep

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from page_objects.MainPage import MainPage
from page_objects.testCases import test_cases
from selenium import webdriver


class TestGmailCases(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path="C:\\ProgramData\\chocolatey\\lib\\chromedriver\\tools\\chromedriver.exe")
        # self.driver = webdriver.Firefox()
        self.driver.maximize_window()

    def test_page_load(self):
        self.driver.get("https://webapp.wisestamp.com/")
        print("\n{0}".format(str(test_cases(0))))
        page = MainPage(self.driver)
        # WebDriverWait(driver, 10).until(lambda d: d.title != "")
        sleep(10);
        self.assertTrue(page.check_page_loaded())

    def test_login_button(self):
        print ("\n" + str(test_cases(2)))
        page = MainPage(self.driver)
        signUpPage = page.click_login_button();

    #       self.assertIn("ap/register", signUpPage.get_url())

    def test_sign_in_button(self):
        print ("\n" + str(test_cases(3)))
        page = MainPage(self.driver)
        loginPage = page.click_sign_in_button()
        self.assertIn("ap/signin", loginPage.get_url())

    def test_sign_in_with_valid_user(self):
        print ("\n" + str(test_cases(4)))
        mainPage = MainPage(self.driver)
        loginPage = mainPage.click_sign_in_button()
        result = loginPage.login_with_valid_user("valid_user")
        self.assertIn("yourstore/home", result.get_url())

    def test_sign_in_with_in_valid_user(self):
        print ("\n" + str(test_cases(5)))
        mainPage = MainPage(self.driver)
        loginPage = mainPage.click_sign_in_button()
        result = loginPage.login_with_in_valid_user("invalid_user")
        self.assertIn("There was a problem with your request", result)

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPages)
    unittest.TextTestRunner(verbosity=2).run(suite)
