# Import the Selenium 2 namespace (aka "webdriver")
from selenium import webdriver

# Google Chrome
from selenium.webdriver.chrome.webdriver import WebDriver

driver = webdriver.Chrome(executable_path="C:\\installs\\chromedriver_win32\\chromedriver.exe")  # type: WebDriver


# ------------------------------
# The actual test scenario: Test the codepad.org code execution service.

# Go to codepad.org
driver.get('http://codepad.org')

# Select the Python language option
python_link = driver.find_elements_by_xpath("//input[@name='lang' and @value='Python']")[0]
python_link.click()

# Enter some text!
text_area = driver.find_element_by_id('textarea')
text_area.send_keys("print 'Hello,' + ' World!'")

# Submit the form!
submit_button = driver.find_elements_by_css_selector('button[data-sitekey="6LeZ-0YUAAAAAMY-yWNlyQ8JtmsarPt94usKI3Dw"]')


driver.get('http://google.com')
textgoogle = driver.find_elements_by_css_selector('input#q')




# Make this an actual test. Isn't Python beautiful?
#assert "Hello, World!" in driver.get_page_source()

# Close the browser!
driver.quit()