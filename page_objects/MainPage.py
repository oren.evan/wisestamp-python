from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

from page_objects.LoginPage import LoginPage
from page_objects.Page import Page


class MainPage(Page):
    def __init__(self, driver):
        super(MainPage, self).__init__(driver)  # Python2 version

  #  LOGIN_BTN = (By.CSS_SELECTOR, 'ws - navigation - top - -login_link')
    LOGIN_BTN = (By.ID, 'ws - navigation - login')

    LOGO = (By.CSS_SELECTOR, '.ws-navigation-top--logo nav_icon_wisestamp')

    def check_page_loaded(self):
        return True if self.find_element(*self.LOGIN_BTN) else False

    def search_item(self, item):
        self.find_element(*self.locator.SEARCH).send_keys(item)
        self.find_element(*self.locator.SEARCH).send_keys(Keys.ENTER)
        return self.find_element(*self.locator.SEARCH_LIST).text

    def click_login_button(self):
        self.find_element(*self.LOGIN_BTN).click()
        return LoginPage(self.driver)
