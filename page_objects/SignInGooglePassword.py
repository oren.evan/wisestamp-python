from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from page_objects.FreeEmailSignatureGenerator import FreeEmailSignatureGenerator
from page_objects.Page import Page


class SignInGooglePassword(Page):
    def __init__(self, driver):
        super(SignInGooglePassword, self).__init__(driver)  # Python2 version

    PASSWORD_NEXT_BTN = (By.CSS_SELECTOR, '#passwordNext')
    LOGO = (By.CSS_SELECTOR, '.service - logo')
    PASSWORD = (By.CSS_SELECTOR, 'input[type=\"password\"]')

    def enter_password(self):
        self.find_element(*self.PASSWORD).send_keys("wavepoint")

    def check_page_loaded(self):
        return True if self.find_element(*self.LOGO) else False

    def click_next_button(self):
        self.find_element(*self.PASSWORD_NEXT_BTN).click()
        # wait to make sure there are two windows open
        WebDriverWait(self.driver, 10).until(lambda d: len(d.window_handles) == 1)
        # switch windows
        self.driver.switch_to_window(self.driver.window_handles[0])
        # wait to make sure the new window is loaded
        WebDriverWait(self.driver, 10).until(lambda d: d.title != "")

        return FreeEmailSignatureGenerator(self.driver)

    def is_selected_password(self):
        self.find_element(*self.PASSWORD).is_selected()
