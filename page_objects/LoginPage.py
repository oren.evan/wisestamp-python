from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from page_objects.SignInGoogleEmail import SignInGoogleEmail
from page_objects.Page import Page


class LoginPage(Page):
    def __init__(self, driver):
       super(LoginPage, self).__init__(driver)  # Python2 version

    LOGIN_GOOGLE_BTN = (By.CSS_SELECTOR, 'div.connect-btns > div.btn.google.transition.scale')
    LOGO = (By.CSS_SELECTOR, '.service - logo')

    EMAIL_TEXT = (By.CSS_SELECTOR, 'input#login_email')
    PASSWORD_TEXT = (By.CSS_SELECTOR, 'input#login_password')
    LOGIN_BTN = (By.CSS_SELECTOR, 'button#login_button')
    ALREADY_GOT_ACCOUNT_BTN = (By.CSS_SELECTOR, '.login-now-btn')
    FORGOT_PASSWORD_LINK = (By.CSS_SELECTOR, 'a[href=\"/forgot_password\"]')
    CREATE_ACCOUNT = (By.CSS_SELECTOR, 'div.form.login > div.form-links > span[ng-click=\"fn.switchForms()\"]')


    def click_login_button(self):
        self.find_element(*self.locator.SUBMIT).click()

    def login(self, user):
        self.enter_email(user)
        self.enter_password(user)
        self.click_login_button()


    def login_with_in_valid_user(self, user):
        self.login(user)
        return self.find_element(*self.locator.ERROR_MESSAGE).text

    def check_page_loaded(self):
        return True if self.find_element(*self.locator.LOGO) else False


    def click_login_google_button(self):
        self.find_element(*self.LOGIN_GOOGLE_BTN).click()
        # wait to make sure there are two windows open
        WebDriverWait(self.driver, 10).until(lambda d: len(d.window_handles) == 2)
        # switch windows
        self.driver.switch_to_window(self.driver.window_handles[1])
        # wait to make sure the new window is loaded
        WebDriverWait(self.driver, 10).until(lambda d: d.title != "")
        return SignInGoogleEmail(self.driver)
